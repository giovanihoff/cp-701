# Nome do executável que será gerado
TARGET = miniature-shell

# Lista de arquivos fonte
SRCS = miniature-shell.c

# Comando para compilar os arquivos fonte
CC = gcc

# Opções de compilação
CFLAGS = -Wall -Wextra -pedantic

# Regra padrão para compilar o programa
all: $(TARGET)

# Regra para compilar o programa
$(TARGET): $(SRCS)
	$(CC) $(CFLAGS) -o $(TARGET) $(SRCS)

# Regra para limpar os arquivos temporários e o executável
clean:
	rm -f $(TARGET)