# CA-701 Computer Systems Engineering

## Prof. Lourenco A Pereira

### The mini-shell project

April 12, 2024

This work is about the implementation of a miniature shell. When started, it shows a prompt command. Example:

`Welcome to the miniature-shell. `

`cmd> _`

The user must provide a full path executable file name corresponding to the command the shell will spawn.
Your mini-shell implementation must use: fork(2) to create a new process; dup2(2) to change the new process’
standard input (stdin) and output (stdout); execv(3) to load the binary image into the memory and execute it.
The parent process (in this case, mini-shell) must wait for the child process (command triggered by the user) to
finish with the wait(2) function.

The mini-shell have two redirect commands:

> *• >: the process’ stdout will be a file. E.g., prog > out.txt, where all the process’ output go to out.txt file;
> *• <: stdin read from a file. E.g., prog < in.txt, where all input data will be read from the in.txt file.**

A way to improve your project is to understand and declare appropriate data structures to handle the user’s
command-line, list of executing process, and others. Here is a good entry-point for this journey: https://www.
gnu.org/software/libc/manual/html_node/Job-Control.html
You must pay special attention to high-level functions. The use of system(3) is not permitted because it
obscures the interactions with operating system calls, which is undesirable. Instead, refer to fork(2) and execv(3)
as outlined in manual section two. This means you should prioritize system-call functions whenever possible.
Adhering to this guidance—preferring functions from manual section two and system calls—will keep you on the
right path. Attention:

1. Utilize DC (it disciplina consciente) and complete the mini-shell project independently. You may brainstorm
   the problem and discuss strategies to overcome challenges, but remember that sharing code and other artifacts
   with your peers is strictly prohibited.
   Some valid inputs to mini-shell:

```
./prog arg1 arg2 argn
/bin/ls
/bin/cat test.txt > out.txt
./prog < in.txt
```

1. Mandatory use of C language. Provide a makefile to compile your mini-shell.
2. Deadline: April 30, 2024. (submitted in classroom)
