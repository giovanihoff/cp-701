/*
    Command Examples:
        /bin/wc -l < in.txt
        /bin/ls -la
        /bin/cat test.txt > out.txt
        /bin/ping -c 3 www.example.com
        ./hello
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>

#define MAX_INPUT_LENGTH 100
#define MAX_ARGS 10

void clear_screen() {
    printf("\033[2J\033[H");
}

void prompt_command(char *command, int *flag) {
    printf("cmd> ");
    fflush(stdout);
    if (fgets(command, MAX_INPUT_LENGTH, stdin) == NULL) {
        perror("Error reading command");
        exit(EXIT_FAILURE);
    }
    command[strcspn(command, "\n")] = '\0';
    if (strcmp(command, "exit") == 0) {
        *flag = 1;
        clear_screen();
        printf("Closing the miniature-shell...\n");
    }
}

void parse_arguments(char *command, char *args[]) {
    char *token = strtok(command, " ");
    int arg_count = 0;
    while (token != NULL && arg_count < MAX_ARGS - 1) {
        args[arg_count++] = token;
        token = strtok(NULL, " ");
    }
    args[arg_count] = NULL;
}

void prepare_redirection(char *args[], int *redirect_input, int *redirect_output, char **input_file, char **output_file) {
    for (int i = 0; args[i] != NULL; i++) {
        if (strcmp(args[i], "<") == 0) {
            *redirect_input = 1;
            *input_file = args[i + 1];
            args[i] = NULL;
        } else if (strcmp(args[i], ">") == 0) {
            *redirect_output = 1;
            *output_file = args[i + 1];
            args[i] = NULL;
        }
    }
}

void redirect_input_file(char *input_file) {
    int fd = open(input_file, O_RDONLY);
    if (fd == -1) {
        perror("Error opening input file");
        exit(EXIT_FAILURE);
    }
    if (dup2(fd, STDIN_FILENO) == -1) {
        perror("Error redirecting input");
        exit(EXIT_FAILURE);
    }
    close(fd);
}

void redirect_output_file(char *output_file) {
    int fd = open(output_file, O_WRONLY | O_CREAT | O_TRUNC, 0666);
    if (fd == -1) {
        perror("Error opening output file");
        exit(EXIT_FAILURE);
    }
    if (dup2(fd, STDOUT_FILENO) == -1) {
        perror("Error redirecting output");
        exit(EXIT_FAILURE);
    }
    close(fd);
}

void execute_child_process(char *args[]) {
    execvp(args[0], args);
    perror("Error executing command");
    exit(EXIT_FAILURE);
}

void execute_command(char *args[]) {
    if (args[0] == NULL) {
        return;
    }

    int redirect_input = 0;
    int redirect_output = 0;
    char *input_file = NULL;
    char *output_file = NULL;

    prepare_redirection(args, &redirect_input, &redirect_output, &input_file, &output_file);

    pid_t pid = fork();
    int status;

    if (pid == -1) {
        perror("Error creating process");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        if (redirect_input) {
            redirect_input_file(input_file);
        }
        if (redirect_output) {
            redirect_output_file(output_file);
        }
        execute_child_process(args);
    } else {
        if (wait(&status) == -1) {
            perror("Error waiting for child process");
            exit(EXIT_FAILURE);
        }
    }
}

int main() {
    char command[MAX_INPUT_LENGTH];
    char *args[MAX_ARGS];
    int flag = 0;

    printf("Welcome to the miniature-shell.\n\n");
    while (1) {
        prompt_command(command, &flag);
        if (flag == 1) {
            break;
        }
        parse_arguments(command, args);
        execute_command(args);
    }

    printf("Bye!\n");
    return 0;
}
